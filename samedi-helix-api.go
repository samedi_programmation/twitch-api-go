package main

import (
	"bufio"
	"fmt"
	"github.com/nicklaw5/helix"
	"os"
	"time"
)

func recupereListeClips(client *helix.Client, broadcasterID string) {

	resp, err := client.GetClips(&helix.ClipsParams{
		BroadcasterID: broadcasterID,
	})

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	fmt.Println()
	fmt.Println("Liste des clips demandée :")
	fmt.Println()
	for _, clip := range resp.Data.Clips {
		fmt.Printf("%s\t%s\t%s\t%s\n", clip.Title, clip.URL, clip.CreatedAt, clip.CreatorName)
	}

	fmt.Println()
	fmt.Printf("Status code: %d\n", resp.StatusCode)
	fmt.Printf("Rate limit: %d\n", resp.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", resp.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", resp.GetRateLimitReset())

	fmt.Println()

}

func getGameIdFromName(client *helix.Client, gameName string) {
	infosJeu, err := client.GetGames(&helix.GamesParams{
		Names: []string{gameName},
	})

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	for _, jeu := range infosJeu.Data.Games {
		fmt.Printf("%s\t%s\n", jeu.ID, jeu.Name)
	}

	fmt.Println()
	fmt.Println()
}

func recupereJeu(client *helix.Client, gameID string) {

	infosJeu, err := client.GetGameAnalytics(&helix.GameAnalyticsParams{
		First:  100,
		GameID: gameID,
		Type:   "overview_v2",
	})

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	fmt.Printf("%+v\n", infosJeu.Data)
	//client.GetGameAnalytics()
	fmt.Println()
	fmt.Printf("Status code: %d\n", infosJeu.StatusCode)
	fmt.Printf("Rate limit: %d\n", infosJeu.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", infosJeu.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", infosJeu.GetRateLimitReset())

	fmt.Println()

}

func recupereListeAbonnes(client *helix.Client, broadcasterID string) {

	subscriptions, err := client.GetSubscriptions(&helix.SubscriptionsParams{
		BroadcasterID: broadcasterID,
	})

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	//fmt.Printf("%+v\n", subscriptions.Data)

	for _, abonne := range subscriptions.Data.Subscriptions {
		fmt.Printf("%s\t%s\t%s\t%B\n", abonne.UserName, abonne.Tier, abonne.PlanName, abonne.IsGift)
	}
	fmt.Println()

	fmt.Println()
	fmt.Printf("Status code: %d\n", subscriptions.StatusCode)
	fmt.Printf("Rate limit: %d\n", subscriptions.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", subscriptions.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", subscriptions.GetRateLimitReset())
	fmt.Println()

}

func TimeIn(t time.Time, name string) (time.Time, error) {
	loc, err := time.LoadLocation(name)
	if err == nil {
		t = t.In(loc)
	}
	return t, err
}

func recupereListeStreamsJeu(client *helix.Client, gameID string, language string) {

	response, err := client.GetStreams(&helix.StreamsParams{
		GameIDs:  []string{gameID},
		Language: []string{language},
	})

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	//fmt.Printf("%+v\n", response.Data)
	fmt.Println()
	for _, stream := range response.Data.Streams {
		heureLocale, _ := TimeIn(stream.StartedAt, "Local")
		fmt.Printf("%s\t%s\t%s\t%s\t%d\n", stream.UserName, stream.Title, stream.Language, heureLocale.String(), stream.ViewerCount)
	}

	fmt.Println()

	fmt.Println()
	fmt.Printf("Status code: %d\n", response.StatusCode)
	fmt.Printf("Rate limit: %d\n", response.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", response.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", response.GetRateLimitReset())
	fmt.Println()


}

func recupereListeVideosJeu(client *helix.Client, userID string, gameID string) {
	response, err := client.GetVideos(&helix.VideosParams{
		UserID: userID,
		GameID: gameID,
	})

	fmt.Printf("%+v\n", response.Data)

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	fmt.Println()

	fmt.Println()
	fmt.Printf("Status code: %d\n", response.StatusCode)
	fmt.Printf("Rate limit: %d\n", response.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", response.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", response.GetRateLimitReset())
	fmt.Println()



}


func recupereListeBan(client *helix.Client, broadcasterID string) {
	response, err := client.GetBannedUsers(&helix.BannedUsersParams{
		BroadcasterID: broadcasterID,
	})
	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	fmt.Printf("%+v\n", response.Data)

	fmt.Println()
	fmt.Printf("Status code: %d\n", response.StatusCode)
	fmt.Printf("Rate limit: %d\n", response.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", response.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", response.GetRateLimitReset())
	fmt.Println()

}

func recupereListeFollow(client *helix.Client, broadcasterID string) []string {

	curseurPagination := ""
	fmt.Println()
	fmt.Println("Liste des follows du streamer : ")
	fmt.Println()

	listeFollows := make([]string, 0)


	for {

		response, err := client.GetUsersFollows(&helix.UsersFollowsParams{
			ToID: broadcasterID,
			After: curseurPagination,
		})

		if err != nil {
			fmt.Errorf("%+v\n", err)
			return nil
		}

		//fmt.Printf("%+v\n", response.Data)
		//fmt.Printf("Pagination : %s\n", response.Data.Pagination.Cursor)

		for _, follower := range response.Data.Follows {
			heureLocale, _ := TimeIn(follower.FollowedAt, "Local")
			chaine := fmt.Sprintf("%s\t%s\t%s\n", follower.FromID, follower.FromName, heureLocale.String())
			listeFollows = append(listeFollows, chaine)
			fmt.Printf(chaine)
		}

		//fmt.Println()
		//fmt.Printf("Status code: %d\n", response.StatusCode)
		//fmt.Printf("Rate limit: %d\n", response.GetRateLimit())
		//fmt.Printf("Rate limit remaining: %d\n", response.GetRateLimitRemaining())
		//fmt.Printf("Rate limit reset: %d\n\n", response.GetRateLimitReset())
		//fmt.Println()

		curseurPagination = response.Data.Pagination.Cursor

		if curseurPagination == "" {
			fmt.Println()
			fmt.Printf("Total : %d\n", response.Data.Total)

			return listeFollows
		}
	}
}

func recupereListeJeuxPlusJoues(client *helix.Client) {

	topGamesResponse, err := client.GetTopGames(&helix.TopGamesParams{})

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	fmt.Println()
	fmt.Println("Liste des jeux les plus joués :")
	fmt.Println()

	for _, game := range topGamesResponse.Data.Games {
		fmt.Printf("%s\t%s\n", game.Name, game.ID)
	}

	fmt.Println()
	fmt.Printf("Status code: %d\n", topGamesResponse.StatusCode)
	fmt.Printf("Rate limit: %d\n", topGamesResponse.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", topGamesResponse.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", topGamesResponse.GetRateLimitReset())
	fmt.Println()

}

func creeClip(client *helix.Client, broadcasterID string) {
	response, err := client.CreateClip(&helix.CreateClipParams{
		BroadcasterID: broadcasterID,
	})

	if err != nil {
		fmt.Errorf("%+v\n", err)
		return
	}

	fmt.Printf("%+v\n", response.Data)

	fmt.Println()
	fmt.Printf("Status code: %d\n", response.StatusCode)
	fmt.Printf("Rate limit: %d\n", response.GetRateLimit())
	fmt.Printf("Rate limit remaining: %d\n", response.GetRateLimitRemaining())
	fmt.Printf("Rate limit reset: %d\n\n", response.GetRateLimitReset())
	fmt.Println()

}


const STREAMER_ID = "182917379"

func main() {
	client, err := helix.NewClient(&helix.Options{
		ClientID:        GetClientId(),
		UserAccessToken: GetToken(),
	})
	if err != nil {
		// handle error
	}

	recupereListeClips(client, STREAMER_ID)
	//fmt.Printf("%+v\n", resp.Data)

	//resp, err = client.GetUsers(&helix.UsersParams{
	//	IDs:    []string{"26301881", "18074328"},
	//	Logins: []string{"summit1g", "lirik", "adaralex"},
	//})
	if err != nil {
		// handle error
	}

	fichier, err := os.Create("liste_follows.txt")
	if err != nil {
		fmt.Println("unable to create file")
		return
	}

	writer := bufio.NewWriter(fichier)


	if err != nil {
		fmt.Println("unable to create file")
		return
	}

	result := recupereListeFollow(client, STREAMER_ID)

	for _, follow := range result {
		_, err := writer.WriteString(follow)
		if err != nil {
			_ = fichier.Close()
		}
	}
	_ = writer.Flush()
	_ = fichier.Close()


	recupereListeJeuxPlusJoues(client)

	getGameIdFromName(client, "Torchlight III")

	recupereJeu(client, "509044")

	recupereListeAbonnes(client, STREAMER_ID)

	recupereListeBan(client, STREAMER_ID)

	recupereListeStreamsJeu(client, "509044", "fr")


	recupereListeVideosJeu(client, STREAMER_ID, "509044")

	//creeClip(client, STREAMER_ID)



	//for _, user := range resp.Data.Users {
	//	fmt.Printf("ID: %s Name: %s\n", user.ID, user.DisplayName)
	//}
}
