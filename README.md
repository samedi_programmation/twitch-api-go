############################
# Les Samedi Programmation #
############################


Samedi 20/06/2020 : Interface avec l'API Helix Twitch
=====================================================


Ce code source permet de montrer comment s'interfacer avec la nouvelle API Twitch (Helix) en Golang.

- Comment récupérer la liste des followers d'une personne
- Comment récupérer la liste des clips d'un streamer
- Comment récupérer la liste des jeux les plus joués à l'heure actuelle sur Twitch
- Comment récupérer l'id Twitch d'un jeu à partir de son nom
- Comment récupérer les infos d'un jeu (à partir de son ID)
- Comment récupérer la liste des abonnés d'un streamer
- Comment récupérer la liste des utilisateurs bans (semble ne pas fonctionner avec l'API ?)
- Comment récupérer la liste des streams live d'un jeu (en fonction de la langue)
- Comment récupérer la liste des vidéos d'un jeu pour un streamer (non fonctionnel, manque les id des vidéos, à récupérer par une ancienne API ?!)



Pour compiler et obtenir un exécutable, go build samedi-helix-api.go token.go

Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex